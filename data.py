from bs4 import BeautifulSoup
import requests


def scrape_poll_data():
    URL = 'https://projects.fivethirtyeight.com/polls/president-general/'

    html_data = requests.get(URL)

    soup = BeautifulSoup(html_data.content, 'html.parser')

    # print(soup.prettify())
    rows = soup.find_all(class_='visible-row')

    pollster_data_array = []
    for row in rows:
        # date_wrapper = row.find(class_='date-wrapper')
        # print(date_wrapper, '\n')
        date = row.find(class_='date-wrapper').text
        # print(date, '\n')

        pollster = row.find(class_='pollster-container')
        # print(pollster, '\n')
        pollster_link = pollster.find_all('a')[-1].text
        # print(pollster_link, '\n')

        sample_size = row.find(class_='sample').text
        # print(sample_size)

        leader = row.find(class_='leader').text
        net = row.find(class_='net').text

        # print(leader, net)
        answers = row.find_all(class_='answer')
        values = row.find_all(class_='value')

        if len(values) == 1:
            next_row = row.findNext("tr")
            value = next_row.find(class_='value')
            answer = next_row.find(class_='answer')

            answers.append(answer)
            values.append(value)

        first_person = answers[0].text
        second_person = answers[1].text

        # print(first_person, second_person, '\n')
        first_value = values[0].find(class_='heat-map').text
        second_value = values[1].find(class_='heat-map').text

        # print(first_value, second_value, '\n')

        # print(answers, values, '\n')

        pollster_data = {
            "date": date,
            "pollster_name": pollster_link,
            "sample_size": sample_size,
            "leader": leader,
            "net": net,
            "first_person": first_person,
            "first_value": first_value,
            "second_person": second_person,
            "second_value": second_value
        }

        pollster_data_array.append(pollster_data)

    return pollster_data_array
